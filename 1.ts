import { writeFileSync } from "fs";
import path from "path";
import { BelongsTo, Column, DataType, ForeignKey, HasMany, Model, Sequelize, Table } from "sequelize-typescript";

// Part 1

const filePathForTask1 = path.resolve(__dirname, "output", "output1.json");

const { ARR_1, ARR_2, EXAM_DATE } = process.env;

const task1 = () => {
  if (typeof ARR_1 !== "string" || ARR_1.length === 0) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент ARR_1 не был передан" }));
    return;
  }

  if (typeof ARR_2 !== "string" || ARR_2.length === 0) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: "Агрумент ARR_2 не был передан" }));
    return;
  }

  const splittedArr1 = ARR_1.split(",");
  const splittedArr2 = ARR_2.split(",");

  const result = [...new Set([...splittedArr1, ...splittedArr2])];

  writeFileSync(
    filePathForTask1,
    JSON.stringify({
      input: {
        ARR_1,
        ARR_2,
      },
      result,
    })
  );
};

task1();

// Part 2
const filePathForTask2 = path.resolve(__dirname, "output", "output2.json");

const initDB = async () => {
  @Table({})
  class Mark extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    lesson: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    fio: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    examDate: string;

    @Column({
      type: DataType.INTEGER,
      allowNull: false,
    })
    mark: number;

    @ForeignKey(() => Student)
    studentId: string;

    @BelongsTo(() => Student, {
      foreignKey: "studentId",
    })
    student!: Student;
  }

  @Table({})
  class Student extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    surname: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    name: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    birthdayDate: Date;

    @HasMany(() => Mark, {
      foreignKey: "studentId",
    })
    markList: Mark[];
  }

  const sequelize = new Sequelize({
    dialect: "postgres",
    host: "database",
    port: 5432,
    username: "postgres",
    password: "qwerty",
    database: "sachkov_practice_db",
  });

  sequelize.addModels([Student, Mark]);

  try {
    await sequelize.authenticate();
    console.log("Init sequelize OK");
  } catch (error) {
    console.error("Init sequelize error", error);
  }

  const studentList = await Student.findAll({
    include: [
      {
        model: Mark,
        required: true,
      },
    ],
  });

  let newExamDate = new Date();
  if (typeof EXAM_DATE === "string" && EXAM_DATE.length > 0) {
    newExamDate = new Date(EXAM_DATE);
  }

  console.log("NewDate =", newExamDate);

  const resultStudentList: Student[] = [];

  studentList.forEach((el) => {
    const badMarkIndex = el.dataValues["markList"].findIndex((mark: any) => {
      const currentExamDate = new Date(mark.dataValues["examDate"]);
      console.log(`ExamCheckDate ${el.dataValues["id"]}`, currentExamDate);
      return currentExamDate.getTime() < newExamDate.getTime();
    });

    if (badMarkIndex === -1) {
      resultStudentList.push(el);
    }
  });

  writeFileSync(filePathForTask2, JSON.stringify(resultStudentList));
};

initDB();
